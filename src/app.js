//Import the UI
var UI = require('ui');

//Create card
var card = new UI.Card({
  title: 'Hello World',
  body: 'This is your first Pebble app!'
});

//Display card
card.show();